import 'package:flutter/material.dart';
import 'package:omni_datetime_picker/omni_datetime_picker.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var key = GlobalKey();

  DateTimeRange? _dateTimeRange;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () async {
                DateTime? dateTime =
                    await showOmniDateTimePicker(context: context);
              },
              child: const Text("Show DateTime Picker"),
            ),
            ElevatedButton(
              onPressed: () async {
                DateTimeRange? dateTimeRange =
                    await showOmniDateTimeRangePicker(
                  context: context,
                  primaryColor: Colors.cyan,
                  backgroundColor: Colors.grey[900],
                  calendarTextColor: Colors.white,
                  tabTextColor: Colors.white,
                  unselectedTabBackgroundColor: Colors.grey[700],
                  buttonTextColor: Colors.white,
                  timeSpinnerTextStyle:
                      const TextStyle(color: Colors.white70, fontSize: 18),
                  timeSpinnerHighlightedTextStyle:
                      const TextStyle(color: Colors.white, fontSize: 24),
                  is24HourMode: true,
                  isShowSeconds: false,
                  // startInitialDate: DateTime.now(),
                  // endInitialDate: DateTime.now(),
                  initialDateTimeRange: _dateTimeRange,
                  startFirstDate:
                      DateTime(1600).subtract(const Duration(days: 3652)),
                  startLastDate: DateTime.now().add(
                    const Duration(days: 3652),
                  ),
                  endFirstDate:
                      DateTime(1600).subtract(const Duration(days: 3652)),
                  endLastDate: DateTime.now().add(
                    const Duration(days: 3652),
                  ),
                  borderRadius: const Radius.circular(16),
                );
              },
              child: const Text("Show DateTime Range Picker"),
            ),
            OmniDateTimeRangeFormField(
              key: key,
              initialValue: _dateTimeRange,
              decoration: const InputDecoration(hintText: 'Date time range'),
              onChanged: (value) {
                setState(() {
                  _dateTimeRange = value;
                });
              },
              // primaryColor: Colors.cyan,
              // backgroundColor: Colors.grey[900],
              // calendarTextColor: Colors.white,
              // tabTextColor: Colors.white,
              // unselectedTabBackgroundColor: Colors.grey[700],
              // buttonTextColor: Colors.white,
              // timeSpinnerTextStyle:
              //     const TextStyle(color: Colors.white70, fontSize: 18),
              // timeSpinnerHighlightedTextStyle:
              //     const TextStyle(color: Colors.white, fontSize: 24),
              is24HourMode: true,
              // isShowSeconds: true,
              startFirstDate:
                  DateTime(1600).subtract(const Duration(days: 3652)),
              startLastDate: DateTime.now().add(
                const Duration(days: 3652),
              ),
              endFirstDate: DateTime(1600).subtract(const Duration(days: 3652)),
              endLastDate: DateTime.now().add(
                const Duration(days: 3652),
              ),
              borderRadius: const Radius.circular(16),
            )
          ],
        ),
      ),
    );
  }
}
