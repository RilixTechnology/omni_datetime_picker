import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutter/rendering.dart';

class ItemScrollPhysics extends ScrollPhysics {
  /// Creates physics for snapping to item.
  /// Based on PageScrollPhysics
  final double? itemHeight;
  final double targetPixelsLimit;

  const ItemScrollPhysics({
    ScrollPhysics? parent,
    this.itemHeight,
    this.targetPixelsLimit = 3.0,
  })  : assert(itemHeight != null && itemHeight > 0),
        super(parent: parent);

  @override
  ItemScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return ItemScrollPhysics(
        parent: buildParent(ancestor), itemHeight: itemHeight);
  }

  double _getItem(ScrollPosition position) {
    double maxScrollItem =
        (position.maxScrollExtent / itemHeight!).floorToDouble();
    return min(max(0, position.pixels / itemHeight!), maxScrollItem);
  }

  double _getPixels(ScrollPosition position, double item) {
    return item * itemHeight!;
  }

  double _getTargetPixels(
      ScrollPosition position, Tolerance tolerance, double velocity) {
    double item = _getItem(position);
    if (velocity < -tolerance.velocity) {
      item -= targetPixelsLimit;
    } else if (velocity > tolerance.velocity) {
      item += targetPixelsLimit;
    }
    return _getPixels(position, item.roundToDouble());
  }

  @override
  Simulation? createBallisticSimulation(
      ScrollMetrics position, double velocity) {
    // If we're out of range and not headed back in range, defer to the parent
    // ballistics, which should put us back in range at a item boundary.
//    if ((velocity <= 0.0 && position.pixels <= position.minScrollExtent) ||
//        (velocity >= 0.0 && position.pixels >= position.maxScrollExtent))
//      return super.createBallisticSimulation(position, velocity);
    Tolerance tolerance = this.tolerance;
    final double target =
        _getTargetPixels(position as ScrollPosition, tolerance, velocity);
    if (target != position.pixels) {
      return ScrollSpringSimulation(spring, position.pixels, target, velocity,
          tolerance: tolerance);
    }
    return null;
  }

  @override
  bool get allowImplicitScrolling => false;
}

typedef SelectedIndexCallback = void Function(int);
typedef TimePickerCallback = void Function(DateTime);

class TimePickerSpinner extends StatefulWidget {
  final DateTime? time;
  final int minutesInterval;
  final int secondsInterval;
  final bool is24HourMode;
  final bool isShowSeconds;
  final TextStyle? highlightedTextStyle;
  final TextStyle? normalTextStyle;
  final double? itemHeight;
  final double? itemWidth;
  final AlignmentGeometry? alignment;
  final double? spacing;
  final bool isForce2Digits;
  final TimePickerCallback onTimeChange;

  const TimePickerSpinner({
    Key? key,
    this.time,
    this.minutesInterval = 1,
    this.secondsInterval = 1,
    this.is24HourMode = true,
    this.isShowSeconds = false,
    this.highlightedTextStyle,
    this.normalTextStyle,
    this.itemHeight,
    this.itemWidth,
    this.alignment,
    this.spacing,
    this.isForce2Digits = false,
    required this.onTimeChange,
  }) : super(key: key);

  @override
  _TimePickerSpinnerState createState() => _TimePickerSpinnerState();
}

class _TimePickerSpinnerState extends State<TimePickerSpinner> {
  ScrollController hourController = ScrollController();
  ScrollController minuteController = ScrollController();
  ScrollController secondController = ScrollController();
  ScrollController apController = ScrollController();
  int currentSelectedHourIndex = -1;
  int currentSelectedMinuteIndex = -1;
  int currentSelectedSecondIndex = -1;
  int currentSelectedAPIndex = -1;
  late DateTime _currentTime;

  /// default settings
  TextStyle defaultHighlightTextStyle =
      const TextStyle(fontSize: 32, color: Colors.black);
  TextStyle defaultNormalTextStyle =
      const TextStyle(fontSize: 32, color: Colors.black54);
  double defaultItemHeight = 60;
  double defaultItemWidth = 45;
  double defaultSpacing = 20;
  AlignmentGeometry defaultAlignment = Alignment.center;

  /// getter

  TextStyle get _highlightedTextStyle =>
      widget.highlightedTextStyle ?? defaultHighlightTextStyle;

  TextStyle get _normalTextStyle =>
      widget.normalTextStyle ?? defaultNormalTextStyle;

  int get _hourCount => widget.is24HourMode ? 24 : 12;

  int get _minuteCount => (60 / widget.minutesInterval).floor();

  int get _secondCount => (60 / widget.secondsInterval).floor();

  double get _itemHeight => widget.itemHeight ?? defaultItemHeight;

  double get _itemWidth => widget.itemWidth ?? defaultItemWidth;

  double get _spacing => widget.spacing ?? defaultSpacing;

  AlignmentGeometry get _alignment => widget.alignment ?? defaultAlignment;

  bool isLoop(int value) {
    return value > 10;
  }

  DateTime getDateTime() {
    int hour = currentSelectedHourIndex - _hourCount;
    if (!widget.is24HourMode && currentSelectedAPIndex == 2) hour += 12;
    int minute = (currentSelectedMinuteIndex -
            (isLoop(_minuteCount) ? _minuteCount : 1)) *
        widget.minutesInterval;
    int second = (currentSelectedSecondIndex -
            (isLoop(_secondCount) ? _secondCount : 1)) *
        widget.secondsInterval;
    return DateTime(_currentTime.year, _currentTime.month, _currentTime.day,
        hour, minute, second);
  }

  @override
  void initState() {
    _currentTime = widget.time ?? DateTime.now();

    currentSelectedHourIndex =
        (_currentTime.hour % (widget.is24HourMode ? 24 : 12)) + _hourCount;
    hourController = ScrollController(
        initialScrollOffset: (currentSelectedHourIndex - 1) * _itemHeight);

    currentSelectedMinuteIndex =
        (_currentTime.minute / widget.minutesInterval).floor() +
            (isLoop(_minuteCount) ? _minuteCount : 1);
    minuteController = ScrollController(
        initialScrollOffset: (currentSelectedMinuteIndex - 1) * _itemHeight);

    currentSelectedSecondIndex =
        (_currentTime.second / widget.secondsInterval).floor() +
            (isLoop(_secondCount) ? _secondCount : 1);
    secondController = ScrollController(
        initialScrollOffset: (currentSelectedSecondIndex - 1) * _itemHeight);

    currentSelectedAPIndex = _currentTime.hour >= 12 ? 2 : 1;
    apController = ScrollController(
        initialScrollOffset: (currentSelectedAPIndex - 1) * _itemHeight);

    super.initState();

    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => widget.onTimeChange(getDateTime()));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> contents = [
      SizedBox(
        width: _itemWidth,
        height: _itemHeight * 3,
        child: spinner(
          hourController,
          _hourCount,
          currentSelectedHourIndex,
          1,
          (index) {
            currentSelectedHourIndex = index;
          },
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(":", style: _highlightedTextStyle),
      ),
      SizedBox(
        width: _itemWidth,
        height: _itemHeight * 3,
        child: spinner(
          minuteController,
          _minuteCount,
          currentSelectedMinuteIndex,
          widget.minutesInterval,
          (index) {
            currentSelectedMinuteIndex = index;
          },
        ),
      ),
    ];

    if (widget.isShowSeconds) {
      contents.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(":", style: _highlightedTextStyle),
        ),
      );
    }
    if (widget.isShowSeconds) {
      contents.add(
        SizedBox(
          width: _itemWidth,
          height: _itemHeight * 3,
          child: spinner(
            secondController,
            _secondCount,
            currentSelectedSecondIndex,
            widget.secondsInterval,
            (index) {
              currentSelectedSecondIndex = index;
            },
          ),
        ),
      );
    }

    if (!widget.is24HourMode) {
      contents.add(spacer());
      contents.add(
        SizedBox(
          width: _itemWidth * 1.2,
          height: _itemHeight * 3,
          child: apSpinner(),
        ),
      );
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: contents,
    );
  }

  Widget spacer() {
    return SizedBox(width: _spacing, height: _itemHeight * 3);
  }

  Widget spinner(ScrollController controller, int max, int selectedIndex,
      int interval, SelectedIndexCallback onUpdateSelectedIndex) {
    /// wrapping the spinner with stack and add container above it when it's scrolling
    /// this thing is to prevent an error causing by some weird stuff like this
    /// flutter: Another exception was thrown: 'package:flutter/src/widgets/scrollable.dart': Failed assertion: line 469 pos 12: '_hold == null || _drag == null': is not true.
    /// maybe later we can find out why this error is happening

    Widget _spinner = NotificationListener<ScrollNotification>(
      onNotification: (scrollNotification) {
        if (scrollNotification is UserScrollNotification) {
          if (scrollNotification.direction == ScrollDirection.idle) {
            if (isLoop(max)) {
              int segment = (selectedIndex / max).floor();
              if (segment == 0) {
                onUpdateSelectedIndex(selectedIndex + max);
                controller.jumpTo(controller.offset + (max * _itemHeight));
              } else if (segment == 2) {
                onUpdateSelectedIndex(selectedIndex - max);
                controller.jumpTo(controller.offset - (max * _itemHeight));
              }
            }
            setState(() => widget.onTimeChange(getDateTime()));
          }
        } else if (scrollNotification is ScrollUpdateNotification) {
          setState(() {
            onUpdateSelectedIndex(
                (controller.offset / _itemHeight).round() + 1);
          });
        }
        return true;
      },
      child: ListView.builder(
        itemBuilder: (context, index) {
          String text = '';
          if (isLoop(max)) {
            text = ((index % max) * interval).toString();
          } else if (index != 0 && index != max + 1) {
            text = (((index - 1) % max) * interval).toString();
          }
          if (!widget.is24HourMode &&
              controller == hourController &&
              text == '0') {
            text = '12';
          }
          if (widget.isForce2Digits && text != '') {
            text = text.padLeft(2, '0');
          }
          return Container(
            height: _itemHeight,
            alignment: _alignment,
            child: Text(
              text,
              style: selectedIndex == index
                  ? _highlightedTextStyle
                  : _normalTextStyle,
            ),
          );
        },
        controller: controller,
        itemCount: isLoop(max) ? max * 3 : max + 2,
        physics: ItemScrollPhysics(itemHeight: _itemHeight),
        padding: EdgeInsets.zero,
      ),
    );

    return _spinner;

    // return Stack(
    //   children: <Widget>[
    //     Positioned.fill(child: _spinner),
    //     isScrolling
    //         ? Positioned.fill(
    //             child: Container(
    //             color: Colors.black.withOpacity(0),
    //           ))
    //         : Container()
    //   ],
    // );
  }

  Widget apSpinner() {
    Widget _spinner = NotificationListener<ScrollNotification>(
      onNotification: (scrollNotification) {
        if (scrollNotification is UserScrollNotification) {
          if (scrollNotification.direction == ScrollDirection.idle) {
            // isAPScrolling = false;
            widget.onTimeChange(getDateTime());
          }
        } else if (scrollNotification is ScrollUpdateNotification) {
          setState(() {
            currentSelectedAPIndex =
                (apController.offset / _itemHeight).round() + 1;
            // isAPScrolling = true;
          });
        }
        return true;
      },
      child: ListView.builder(
        itemBuilder: (context, index) {
          String text = index == 1 ? 'AM' : (index == 2 ? 'PM' : '');
          return Container(
            height: _itemHeight,
            alignment: Alignment.center,
            child: Text(
              text,
              style: currentSelectedAPIndex == index
                  ? _highlightedTextStyle
                  : _normalTextStyle,
            ),
          );
        },
        controller: apController,
        itemCount: 4,
        physics: ItemScrollPhysics(
          itemHeight: _itemHeight,
          targetPixelsLimit: 1,
        ),
      ),
    );

    return _spinner;
    // return Stack(
    //   children: <Widget>[
    //     Positioned.fill(child: _spinner),
    //     isAPScrolling ? Positioned.fill(child: Container()) : Container()
    //   ],
    // );
  }
}
