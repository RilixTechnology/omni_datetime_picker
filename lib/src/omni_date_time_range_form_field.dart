import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'omni_datetime_range_picker.dart';

/// A [OmniDateTimeRangeFormField] which extends a [FormField].
///
/// The use of a [Form] ancestor is not required, however it makes it easier to
/// save, reset, and validate multiple fields at the same time. In order to use
/// this without a [Form] ancestor, pass a [GlobalKey] to the constructor and use
/// [GlobalKey.currentState] the same way as you would for a form.
///
/// To style this widget, pass an [InputDecoration] to the constructor. If not,
/// the [OmniDateTimeRangeFormField] will use the default from the [Theme].
///
/// This widget must have a [Material] ancestor, such as a [MaterialApp] or [Form].
class OmniDateTimeRangeFormField extends FormField<DateTimeRange> {
  final  DateTimeRange? initialValue;
  final DateTime? startFirstDate;
  final DateTime? startLastDate;
  final DateTime? endFirstDate;
  final DateTime? endLastDate;
  final bool is24HourMode;
  final bool isShowSeconds;
  final Color? primaryColor;
  final Color? backgroundColor;
  final Color? calendarTextColor;
  final Color? tabTextColor;
  final Color? unselectedTabBackgroundColor;
  final Color? buttonTextColor;
  final TextStyle? timeSpinnerTextStyle;
  final TextStyle? timeSpinnerHighlightedTextStyle;
  final Radius? borderRadius;

  // final bool enabled;
  final double? width;
  final EdgeInsets? margin;

  // final DateTimeRange? initialValue;
  final DateFormat? dateFormat;
  final TextStyle? textStyle;

  /// Creates a [OmniDateTimeRangeFormField] which extends a [FormField].
  ///
  /// When using without a [Form] ancestor a [GlobalKey] is required.
  OmniDateTimeRangeFormField(
      {Key? key,
      this.initialValue,
      this.startFirstDate,
      this.startLastDate,
      this.endFirstDate,
      this.endLastDate,
      this.is24HourMode = false,
      this.isShowSeconds = false,
      this.primaryColor,
      this.backgroundColor,
      this.calendarTextColor,
      this.tabTextColor,
      this.unselectedTabBackgroundColor,
      this.buttonTextColor,
      this.timeSpinnerTextStyle,
      this.timeSpinnerHighlightedTextStyle,
      this.borderRadius,
      enabled = true,
      this.width,
      this.margin,
      ValueChanged<DateTimeRange?>? onChanged,
      FormFieldSetter<DateTimeRange>? onSaved,
      FormFieldValidator<DateTimeRange>? validator,
      bool autoValidate = false,
      this.dateFormat,
      this.textStyle,
      InputDecoration decoration = const InputDecoration()})
      : super(
          key: key,
          validator: validator,
          onSaved: onSaved,
          enabled: enabled,
          initialValue: initialValue,
          builder: (FormFieldState<DateTimeRange> state) {
            final format = (dateFormat ?? DateFormat('dd/MM/yyyy, HH:mm'));
            final InputDecoration inputDecoration = decoration
                .copyWith(enabled: enabled)
                .applyDefaults(Theme.of(state.context).inputDecorationTheme);

            /// This is the dialog to select the date range.
            Future<void> selectDateRange() async {
              DateTimeRange? picked = await showDialog(
                context: state.context,
                builder: (BuildContext context) {
                  return OmniDateTimeRangePicker(
                    initialDateTimeRange: initialValue,
                    startFirstDate: startFirstDate,
                    startLastDate: startLastDate,
                    endFirstDate: endFirstDate,
                    endLastDate: endLastDate,
                    is24HourMode: is24HourMode,
                    isShowSeconds: isShowSeconds,
                    primaryColor: primaryColor,
                    backgroundColor: backgroundColor,
                    calendarTextColor: calendarTextColor,
                    tabTextColor: tabTextColor,
                    unselectedTabBackgroundColor: unselectedTabBackgroundColor,
                    buttonTextColor: buttonTextColor,
                    timeSpinnerTextStyle: timeSpinnerTextStyle,
                    timeSpinnerHighlightedTextStyle:
                        timeSpinnerHighlightedTextStyle,
                    borderRadius: borderRadius,
                  );
                },
              );

              if (picked != null && picked != state.value) {
                state.didChange(picked);
                onChanged?.call(picked);
              }
            }

            String hintText = decoration.hintText ?? '';
            return InkWell(
              /// This calls the dialog to select the date range.
              onTap: enabled ? selectDateRange : null,
              child: Container(
                margin: margin,
                width: width ?? MediaQuery.of(state.context).size.width,
                child: InputDecorator(
                  decoration:
                      inputDecoration.copyWith(errorText: state.errorText),
                  child: Text(
                    // This will display hintText if provided and if state.value is null
                    state.value == null
                        ? hintText
                        :

                        /// This displays the selected date range when the dialog is closed.
                        '${format.format(state.value!.start)} - ${format.format(state.value!.end)}',
                    style: (state.value == null &&
                            hintText != '' &&
                            decoration.hintStyle != null)
                        ? decoration.hintStyle
                        : textStyle,
                  ),
                ),
              ),
            );
          },
        );
}
