import 'package:flutter/material.dart';

import 'time_picker_spinner.dart';

class DateTimePickerView extends StatefulWidget {
  final DateTime initialDateTime;
  final DateTime firstDate;
  final DateTime lastDate;
  final TextStyle timeSpinnerHighlightedTextStyle;
  final TextStyle timeSpinnerTextStyle;
  final bool is24HourMode;
  final bool isShowSeconds;
  final Function(DateTime) onDateTimeChanged;

  const DateTimePickerView({
    Key? key,
    required this.initialDateTime,
    required this.firstDate,
    required this.lastDate,
    required this.timeSpinnerHighlightedTextStyle,
    required this.timeSpinnerTextStyle,
    required this.is24HourMode,
    required this.isShowSeconds,
    required this.onDateTimeChanged,
  }) : super(key: key);

  @override
  State<DateTimePickerView> createState() => _DateTimePickerViewState();
}

class _DateTimePickerViewState extends State<DateTimePickerView>
    with AutomaticKeepAliveClientMixin<DateTimePickerView> {

  late DateTime _startDateTime;
  
  @override
  void initState() {
    _startDateTime = widget.initialDateTime;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CalendarDatePicker(
            initialDate: _startDateTime,
            firstDate: widget.firstDate,
            lastDate: widget.lastDate,
            onDateChanged: (dateTime) {
              debugPrint("CalendarDatePicker onTimeChange: $dateTime");
              _startDateTime = DateTime(
                dateTime.year,
                dateTime.month,
                dateTime.day,
                _startDateTime.hour,
                _startDateTime.minute,
              );
              widget.onDateTimeChanged(_startDateTime);
            },
          ),
          TimePickerSpinner(
            is24HourMode: widget.is24HourMode,
            isForce2Digits: widget.is24HourMode,
            isShowSeconds: widget.isShowSeconds,
            normalTextStyle: widget.timeSpinnerTextStyle,
            highlightedTextStyle: widget.timeSpinnerHighlightedTextStyle,
            time: _startDateTime,
            onTimeChange: (dateTime) {
              debugPrint("TimePickerSpinner startDate: $_startDateTime");
              debugPrint("TimePickerSpinner dateTime: $dateTime");
              _startDateTime = DateTime(
                _startDateTime.year,
                _startDateTime.month,
                _startDateTime.day,
                dateTime.hour,
                dateTime.minute,
                dateTime.second,
              );
              debugPrint("TimePickerSpinner after: $_startDateTime");
              widget.onDateTimeChanged(_startDateTime);
            },
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
