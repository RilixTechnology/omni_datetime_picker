import 'package:flutter/material.dart';
import 'package:omni_datetime_picker/src/date_time_picker_view.dart';

/// Omni DateTimeRange Picker
/// If properties are not given, default value will be used.
class OmniDateTimeRangePicker extends StatefulWidget {
  /// Start initial datetime
  /// Default value: DateTime.now()
  // final DateTime? startDateTime;
  /// End initial datetime
  /// Default value: DateTime.now().add(const Duration(days: 1))
  // final DateTime? endDateTime;
  final DateTimeRange? initialDateTimeRange;

  /// Minimum date that can be selected
  ///
  /// Default value: DateTime.now().subtract(const Duration(days: 3652))
  final DateTime? startFirstDate;

  /// Maximum date that can be selected
  ///
  /// Default value: DateTime.now().add(const Duration(days: 3652))
  final DateTime? startLastDate;

  /// Minimum date that can be selected
  ///
  /// Default value: DateTime.now().subtract(const Duration(days: 3652))
  final DateTime? endFirstDate;

  /// Maximum date that can be selected
  ///
  /// Default value: DateTime.now().add(const Duration(days: 3652))
  final DateTime? endLastDate;

  final bool is24HourMode;
  final bool isShowSeconds;

  final Color? primaryColor;
  final Color? backgroundColor;
  final Color? calendarTextColor;
  final Color? tabTextColor;
  final Color? unselectedTabBackgroundColor;
  final Color? buttonTextColor;
  final TextStyle? timeSpinnerTextStyle;
  final TextStyle? timeSpinnerHighlightedTextStyle;
  final Radius? borderRadius;

  const OmniDateTimeRangePicker({
    Key? key,
    // this.startDateTime,
    // this.endDateTime,
    this.initialDateTimeRange,
    this.startFirstDate,
    this.endFirstDate,
    this.startLastDate,
    this.endLastDate,
    this.is24HourMode = false,
    this.isShowSeconds = false,
    this.primaryColor,
    this.backgroundColor,
    this.calendarTextColor,
    this.tabTextColor,
    this.unselectedTabBackgroundColor,
    this.buttonTextColor,
    this.timeSpinnerTextStyle,
    this.timeSpinnerHighlightedTextStyle,
    this.borderRadius,
  }) : super(key: key);

  @override
  State<OmniDateTimeRangePicker> createState() =>
      _OmniDateTimeRangePickerState();
}

class _OmniDateTimeRangePickerState extends State<OmniDateTimeRangePicker>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late MaterialLocalizations _localizations;

  late DateTime _startDateTime;
  late DateTime _endDateTime;

  late BorderRadius _borderRadius;
  late BorderRadius _buttonBorderRadius;
  late Color _backgroundColor;
  late Color _primaryColor;
  late Color _unselectedTabBackgroundColor;

  late final TextStyle _tabLabelTextStyle;
  late final TextStyle _timeSpinnerTextStyle;
  late final TextStyle _timeSpinnerHighlightedTextStyle;
  late final Color _buttonTextColor;

  late DateTime _startFirstDate;
  late DateTime _startLastDate;
  late DateTime _endFirstDate;
  late DateTime _endLastDate;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      setState(() {});
    });
    if(widget.initialDateTimeRange == null) {
      _startDateTime = DateTime.now();
      _endDateTime = DateTime.now().add(const Duration(days: 1));
    } else {
      _startDateTime = widget.initialDateTimeRange!.start;
      _endDateTime = widget.initialDateTimeRange!.end;
    }

    _borderRadius = BorderRadius.only(
      topLeft: widget.borderRadius ?? const Radius.circular(16),
      topRight: widget.borderRadius ?? const Radius.circular(16),
    );
    _buttonBorderRadius = BorderRadius.only(
      bottomLeft: widget.borderRadius ?? const Radius.circular(16),
      bottomRight: widget.borderRadius ?? const Radius.circular(16),
    );

    _backgroundColor = widget.backgroundColor ?? Colors.white;
    _primaryColor = widget.primaryColor ?? Colors.blue;
    _unselectedTabBackgroundColor =
        widget.unselectedTabBackgroundColor ?? Colors.grey[200]!;

    _tabLabelTextStyle =
        TextStyle(color: widget.tabTextColor ?? Colors.black87);

    _timeSpinnerTextStyle = widget.timeSpinnerTextStyle ??
        TextStyle(
          fontSize: 18,
          color: widget.calendarTextColor ?? Colors.black54,
        );

    _timeSpinnerHighlightedTextStyle = widget.timeSpinnerHighlightedTextStyle ??
        TextStyle(
          fontSize: 24,
          color: widget.calendarTextColor ?? Colors.black,
        );

    _buttonTextColor = widget.buttonTextColor ?? Colors.black;

    _startFirstDate = widget.startFirstDate ??
        DateTime.now().subtract(const Duration(days: 3652));
    _startLastDate =
        widget.startLastDate ?? DateTime.now().add(const Duration(days: 3652));
    _endFirstDate = widget.endFirstDate ??
        DateTime.now().subtract(const Duration(days: 3652));
    _endLastDate =
        widget.endLastDate ?? DateTime.now().add(const Duration(days: 3652));
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _localizations = MaterialLocalizations.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      alignment: Alignment.center,
      child: Theme(
        data: ThemeData(
          colorScheme: Theme.of(context).colorScheme.copyWith(
                primary: _primaryColor,
                surface: _backgroundColor,
                onSurface: widget.calendarTextColor ?? Colors.black,
              ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TabBar(
              controller: _tabController,
              padding: EdgeInsets.zero,
              labelPadding: EdgeInsets.zero,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: _backgroundColor,
              indicatorWeight: 0.1,
              tabs: [
                Container(
                  height: 48,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: _tabController.index == 0
                        ? _backgroundColor
                        : _unselectedTabBackgroundColor,
                    borderRadius: _borderRadius,
                  ),
                  child: Text(
                    _localizations.dateRangeStartLabel,
                    style: _tabLabelTextStyle,
                  ),
                ),
                Container(
                  height: 48,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: _tabController.index == 1
                        ? _backgroundColor
                        : _unselectedTabBackgroundColor,
                    borderRadius: _borderRadius,
                  ),
                  child: Text(
                    _localizations.dateRangeEndLabel,
                    style: _tabLabelTextStyle,
                  ),
                ),
              ],
            ),
            Container(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height - 180),
              decoration: BoxDecoration(color: _backgroundColor),
              child: TabBarView(
                controller: _tabController,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  /// Start date
                  DateTimePickerView(
                    initialDateTime: _startDateTime,
                    firstDate: _startFirstDate,
                    lastDate: _startLastDate,
                    timeSpinnerHighlightedTextStyle:
                        _timeSpinnerHighlightedTextStyle,
                    timeSpinnerTextStyle: _timeSpinnerTextStyle,
                    is24HourMode: widget.is24HourMode,
                    isShowSeconds: widget.isShowSeconds,
                    onDateTimeChanged: (dateTime) {
                      setState(() => _startDateTime = dateTime);
                    },
                  ),

                  /// End date
                  DateTimePickerView(
                    initialDateTime: _endDateTime,
                    firstDate: _endFirstDate,
                    lastDate: _endLastDate,
                    timeSpinnerHighlightedTextStyle:
                    _timeSpinnerHighlightedTextStyle,
                    timeSpinnerTextStyle: _timeSpinnerTextStyle,
                    is24HourMode: widget.is24HourMode,
                    isShowSeconds: widget.isShowSeconds,
                    onDateTimeChanged: (dateTime) {
                      setState(() => _endDateTime = dateTime);
                    },
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(7),
              decoration: BoxDecoration(
                color: _backgroundColor,
                borderRadius: _buttonBorderRadius,
              ),
              child: ClipRRect(
                borderRadius: _buttonBorderRadius,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(_backgroundColor),
                        ),
                        onPressed: () => Navigator.pop(context),
                        child: Text(
                          _localizations.cancelButtonLabel,
                          style: TextStyle(color: _buttonTextColor),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                      child: VerticalDivider(color: Colors.grey),
                    ),
                    Expanded(
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(_backgroundColor),
                        ),
                        onPressed: () {
                          Navigator.pop<DateTimeRange>(
                            context,
                            DateTimeRange(
                              start: _startDateTime,
                              end: _endDateTime,
                            ),
                          );
                        },
                        child: Text(
                          _localizations.saveButtonLabel,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: _buttonTextColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
